from pathlib import Path
from sklearn import preprocessing
import pandas as pd
from dataset import CifarDataset
from model import SimpleCNN
from torchvision.transforms import transforms
from typing import Callable, Tuple

import torch.nn.functional as F
import torch.nn as nn
import torch
from tqdm import tqdm


DATASET_PATH = "torch_base_homework\data\cifar10\cifar10"
EPOCHS = 1
SAVE_MODEL_PATH = Path("torch_base_homework/models/model.ckpt")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
transform = transforms.Compose(
    [
        transforms.RandomCrop(32, padding=4),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
    ]
)


def get_dataframe(data_path: Path):
    labels = ["cat", "dog", "mouse", "elephant", "pandas"]
    le = preprocessing.LabelEncoder()
    targets = le.fit_transform(labels)

    df = pd.DataFrame({"image": data_path.rglob("*.png")})
    df["label"] = df["image"].apply(lambda p: str(p.parents[0].name))
    df["split"] = df["image"].apply(lambda p: str(p.parents[1].name))
    df["int_label"] = le.fit_transform(df["label"])
    return df


def train_one_epoch(
    model: nn.Module,
    dataloader: torch.utils.data.DataLoader,
    optimizer: torch.optim.Optimizer,
    criterion: Callable,
) -> Tuple[float, float]:
    # помним, что некоторые модули ведут себя по разному во время инференса и обучения
    # например Dropout, BatchNormalization
    model.train()

    train_running_loss = 0.0
    train_running_correct = 0
    total_epoch_steps = int(len(dataloader.dataset) / dataloader.batch_size)

    for _, batch in tqdm(enumerate(dataloader), total=total_epoch_steps):
        images, target = batch
        images, target = images.to(device), target.to(device)
        target = target.type(torch.LongTensor)
        outputs = model(images)

        # print(f"{outputs=}")
        # print(f"{target=}")
        loss = criterion(outputs, target)
        train_running_loss += loss.item()

        _, preds = torch.max(
            outputs.data, 1
        )  # можем не использовать softmax, а просто взять .max
        train_running_correct += (preds == target).sum().item()

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    train_loss = train_running_loss / len(dataloader.dataset)
    train_accuracy = 100.0 * train_running_correct / len(dataloader.dataset)
    return train_loss, train_accuracy


def validate(
    model: nn.Module,
    dataloader: torch.utils.data.DataLoader,
    criterion: Callable,
):
    model.eval()

    val_running_loss = 0.0
    val_running_correct = 0

    with torch.no_grad():  # Не нужно считать градиенты (+ к скорости инференса и - потребление памяти)
        inference_steps = int(len(dataloader.dataset) / dataloader.batch_size)

        for _, batch in tqdm(enumerate(dataloader), total=inference_steps):
            images, target = batch
            images = images.to(device)
            target = target.to(device)
            target = target.type(torch.LongTensor)

            outputs = model(images)
            loss = criterion(outputs, target)

            val_running_loss += loss.item()
            _, preds = torch.max(outputs.data, 1)
            val_running_correct += (preds == target).sum().item()

        val_loss = val_running_loss / len(dataloader.dataset)
        val_accuracy = 100.0 * val_running_correct / len(dataloader.dataset)
        return val_loss, val_accuracy


def save_model(
    save_path: Path, current_epoch: int, model: SimpleCNN, optimizer: Callable
):
    torch.save(
        {
            "epoch": current_epoch,
            "model_state_dict": model.state_dict(),
            "optimizer_state_dict": optimizer.state_dict(),
        },
        save_path,
    )


def main():
    df = get_dataframe(Path(DATASET_PATH))

    train_dataset = CifarDataset(df, mode="train", transforms=transform)
    test_dataset = CifarDataset(df, mode="test", transforms=transform)
    model = SimpleCNN()

    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        batch_size=128,
        shuffle=True,
        drop_last=True,
        num_workers=2,
        pin_memory=True,
    )

    test_loader = torch.utils.data.DataLoader(
        test_dataset,
        batch_size=1,
        shuffle=False,
        num_workers=2,
        pin_memory=True,
    )

    learning_rate = 1e-4
    optimizer = torch.optim.AdamW(model.parameters(), lr=learning_rate)
    criterion = nn.CrossEntropyLoss()
    train_loss, train_accuracy = [], []

    for epoch in range(EPOCHS):
        print(f"Epoch {epoch+1} of {EPOCHS}")
        train_epoch_loss, train_epoch_accuracy = train_one_epoch(
            model, train_loader, optimizer, criterion
        )

        train_loss.append(train_epoch_loss)
        train_accuracy.append(train_epoch_accuracy)

        print(
            f"Train Loss: {train_epoch_loss:.4f}, Train Acc: {train_epoch_accuracy:.2f}"
        )

        val_loss, val_accuracy = validate(model, test_loader, criterion)
        print(f"Test Loss: {val_loss:.4f}, Test Acc: {val_accuracy:.2f}")
    save_model(SAVE_MODEL_PATH, EPOCHS, model, optimizer)


if __name__ == "__main__":
    main()
