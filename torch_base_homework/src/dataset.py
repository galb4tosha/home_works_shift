import torch
from torch.utils.data import Dataset
from PIL import Image
from typing import Tuple
from torchvision.transforms import transforms
import pandas as pd
from pathlib import Path


# Pytorch data tutorials:
# 1. https://pytorch.org/tutorials/beginner/basics/data_tutorial.html
# 2. https://pytorch.org/docs/stable/data.html


class DatasetTemplate(Dataset):
    """One data point abstraction."""

    def __init__(self):
        """Prepare data (read, download, prepare transforms etc.)"""
        pass

    def __getitem__(self, index):
        """Return single data point (inputs and targets)"""
        pass

    def __len__(self):
        """Return data length"""
        pass


class CifarDataset(Dataset):
    def __init__(self, df: pd.DataFrame, mode="train", transforms=None):
        self.df = df[df.split == mode]
        self.transforms = transforms

    def __getitem__(self, index: int) -> Tuple[torch.Tensor, int]:
        # Нужно пытаться сделать извлечение элеманта максимально быстро
        # Пример: можно заранее порезать большие изображения на патчи
        row = self.df.iloc[index, :]
        image_path = Path(row["image"])
        image = Image.open(image_path)
        label = row["int_label"]

        if self.transforms:
            # аугментируем изображения на CPU
            image = self.transforms(image)

        # Не рекомендуется перекидывать данные на GPU в Dataset!
        return image, label

    def __len__(self):
        return len(self.df)
