from tqdm import tqdm
from dataclasses import dataclass, asdict
from pathlib import Path
import json
import pandas as pd
from time import time


@dataclass
class PosCashBalanceIDs:
    SK_ID_PREV: int
    SK_ID_CURR: int
    NAME_CONTRACT_STATUS: str


@dataclass
class AmtCredit:
    CREDIT_CURRENCY: str
    AMT_CREDIT_MAX_OVERDUE: float
    AMT_CREDIT_SUM: float
    AMT_CREDIT_SUM_DEBT: float
    AMT_CREDIT_SUM_LIMIT: float
    AMT_CREDIT_SUM_OVERDUE: float
    AMT_ANNUITY: float


PATH_TO_RAW_DATA = Path("homework_1/part_1/data/POS_CASH_balance_plus_bureau-001.log")
OUT_CSV_BUREAU = Path("homework_1/part_1/data/bureau.csv")
OUT_CSV_POS_CASH = Path("homework_1/part_1/data/POS_CASH_balance.csv")
BUREAU_TYPE = "bureau"
POS_CASH_BALANCE_TYPE = "POS_CASH_balance"


def parse_bereau_data(data: json) -> list[int | str]:
    """Функция, преобразующая json типа bureau в массив выходного формата

    Args:
        data (json): json, который необходмо распарсить

    Returns:
        list[int | str]: данные в требуемом формате
    """
    data = data["data"]
    data["record"]["AmtCredit"] = eval(data["record"]["AmtCredit"])
    data_list = []
    for key, item in data.items():
        if key == "record":
            for key_record, item_key in data[key].items():
                if key_record == "AmtCredit":
                    amt_as_dict = asdict(data[key][key_record])
                    for _, items_amt_credit in amt_as_dict.items():
                        data_list.append(items_amt_credit)
                else:
                    data_list.append(item_key)
        else:
            data_list.append(item)
    return data_list


def parse_pos_cash_balance_data(data: json) -> list[int | str]:
    """Функция, преобразующая json типа pos cash balance в массив выходного формата

    Args:
        data (json): json, который необходмо распарсить

    Returns:
        list[int | str]: данные в требуемом формате
    """
    data_list = []
    data = data["data"]
    cnt_instalment = data["CNT_INSTALMENT"]
    for record in data["records"]:
        row_list = [cnt_instalment]
        for key, items in record.items():
            if key == "PosCashBalanceIDs":
                pos_cash_balance_id_dict = asdict(eval(items))
                for _, pos_cash_balance_id_items in pos_cash_balance_id_dict.items():
                    row_list.append(pos_cash_balance_id_items)
            else:
                row_list.append(items)
        data_list.append(row_list)
    return data_list


def main():
    """ """
    start_time = time()
    # bureau_data = []
    # pos_cash_data = []
    with open(PATH_TO_RAW_DATA, "r") as f:
        f_bureau = open(OUT_CSV_BUREAU, "w")
        f_pos_cash = open(OUT_CSV_POS_CASH, "w")
        headers = """CREDIT_TYPE,SK_ID_CURR,SK_ID_BUREAU,CREDIT_ACTIVE,DAYS_CREDIT,
                CREDIT_DAY_OVERDUE,DAYS_CREDIT_ENDDATE,DAYS_ENDDATE_FACT,CNT_CREDIT_PROLONG,
                DAYS_CREDIT_UPDATE,CREDIT_CURRENCY,AMT_CREDIT_MAX_OVERDUE,AMT_CREDIT_SUM,
                AMT_CREDIT_SUM_DEBT,AMT_CREDIT_SUM_LIMIT,AMT_CREDIT_SUM_OVERDUE,AMT_ANNUITY""".replace(
            "\n", ""
        ).replace(
            " ", ""
        )
        f_bureau.write(headers + "\n")
        headers = """CNT_INSTALMENT,CNT_INSTALMENT_FUTURE,MONTHS_BALANCE,SK_ID_PREV,
                      SK_ID_CURR,NAME_CONTRACT_STATUS,SK_DPD,SK_DPD_DEF""".replace(
            "\n", ""
        ).replace(
            " ", ""
        )
        f_pos_cash.write(headers + "\n")
        for line in tqdm(f):
            line_json = json.loads(line)
            if line_json["type"] == BUREAU_TYPE:
                line = parse_bereau_data(line_json)
                f_bureau.write(str.join(",", map(lambda x: str(x), line)) + "\n")
            elif line_json["type"] == POS_CASH_BALANCE_TYPE:
                lines = parse_pos_cash_balance_data(line_json)
                for line in lines:
                    f_pos_cash.write(str.join(",", map(lambda x: str(x), line)) + "\n")
            else:
                print(line_json["type"])
        f_bureau.close()
        f_pos_cash.close()
    print(f"work time {time() - start_time}")


if __name__ == "__main__":
    main()
