# import tensorflow as tf
from typing import Tuple

import cv2
import numpy as np
from keras.utils import Sequence
from pandas import DataFrame


class DataGenerator(Sequence):
    def __init__(self, batch_size: int, df: DataFrame, rgb: bool = False) -> None:
        self.batch_size = batch_size
        self.indexes = list(df.index)
        self.df = df
        self.rgb = rgb

    def read_image_gray(self, path: str) -> np.ndarray:
        return cv2.cvtColor(cv2.imread(path), cv2.COLOR_BGR2GRAY)

    def __len__(self) -> int:
        return len(self.indexes) // self.batch_size

    def __getitem__(self, i: int) -> Tuple[np.ndarray]:
        indexes = self.indexes[i * self.batch_size : (i + 1) * self.batch_size]
        images_batch = []
        labels_batch = []
        for j in indexes:
            if self.rgb:
                img = cv2.imread(self.df["filename"][j]) / 255.0
            else:
                img = self.read_image_gray(self.df["filename"][j]) / 255.0
            images_batch.append(img)
            label = self.df["blur_class"][j]
            labels_batch.append(label)
        return np.array(images_batch), np.array(labels_batch)
