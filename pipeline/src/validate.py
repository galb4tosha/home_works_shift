from config.constants import *
from src.train import get_data
from tensorflow.keras.models import load_model

if __name__ == "__main__":
    _, val_dataset_gray = get_data()
    simple_model = load_model(CHECKPOINT_MODEL)
    result_simple = simple_model.evaluate(val_dataset_gray)
