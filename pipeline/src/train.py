from typing import Tuple

import numpy as np
import pandas as pd
from tensorflow.keras import Input, Model
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.layers import Conv2D, Dense, Flatten, MaxPooling2D
from wandb.keras import WandbMetricsLogger, WandbModelCheckpoint

import wandb
from config.constants import *
from src.data_loader import DataGenerator


def get_simple_cnn_model() -> Model:
    """Функция, которая инициализирует примитивную свёрточную сетку

    Returns:
        Model: Свёрточная сетка
    """
    input = Input(shape=IMG_SIZE)
    x = Conv2D(FIRST_LAYER_FILTERS, CNN_KERNEL_SIZE, activation=CNN_LAYERS_ACTIVATION)(
        input
    )
    x = MaxPooling2D()(x)

    for filter in FILTERS[1:-1]:
        x = Conv2D(filter, CNN_KERNEL_SIZE, activation=CNN_LAYERS_ACTIVATION)(x)
        x = MaxPooling2D()(x)

    x = Conv2D(THIRD_LAYER_FILTERS, CNN_KERNEL_SIZE, activation=CNN_LAYERS_ACTIVATION)(
        x
    )
    x = Flatten()(x)

    output = Dense(1, activation=DENSE_LAYER_ACTIVATION)(x)
    model = Model(inputs=input, outputs=output)
    print(model.summary())
    return model


def get_data() -> Tuple[DataGenerator]:
    """Функция, которая инициализирует датасет и возвращает генератор с тренировочным и валидационным датасетами

    Returns:
        Tuple[dataGenerator]: тренировочный и валидационный датасеты
    """
    df_train = pd.read_csv(DF_TRAIN)
    df_train["filename"] = df_train["filename"].apply(
        lambda x: str(TRAIN_PATH.joinpath(x))
    )
    df_train["blur_class"] = df_train["blur"].apply(lambda x: int(x))
    mask = np.random.rand(len(df_train)) < TRAIN_VAL_SPLIT  # split dataset
    train_dataset = df_train[mask]
    val_dataset = df_train[~mask]
    test_imgs = []
    for file in TEST_PATH.rglob("*.jpg"):
        test_imgs.append({"filename": file})
    train_dataset_gray = DataGenerator(BATCH_SIZE, train_dataset)
    val_dataset_gray = DataGenerator(BATCH_SIZE, val_dataset)
    return train_dataset_gray, val_dataset_gray


def train_model(
    model: Model, train_dataset: DataGenerator, val_dataset: DataGenerator
) -> Model:
    """Функция, которая обучает модель на входных данных

    Args:
        model (Model): модель для обучения
        train_dataset (DataGenerator): тренировочный набор данных
        val_dataset (DataGenerator): валидационный набор данных

    Returns:
        Model: обученная модель
    """
    model.compile(loss=MODEL_LOSS, optimizer=OPTIMIZER, metrics=TRACKED_METRICS)
    checkpoint = ModelCheckpoint(
        CHECKPOINT_MODEL,
        monitor=CHECKPOINT_MONITOR,
        mode=CHECKPOINT_MODE,
        save_best_only=True,
    )
    if USE_WANDB:
        history = model.fit(
            train_dataset,
            validation_data=val_dataset,
            epochs=EPOCHS,
            callbacks=[
                WandbMetricsLogger(log_freq=5),
                WandbModelCheckpoint("models"),
                checkpoint,
            ],
        )
    else:
        history = model.fit(
            train_dataset,
            validation_data=val_dataset,
            epochs=EPOCHS,
            callbacks=[checkpoint],
        )
    print(history.history)
    return model


if __name__ == "__main__":
    if USE_WANDB:
        wandb.init(project="pipeline")
    train_dataset_gray, val_dataset_gray = get_data()
    model = get_simple_cnn_model()
    model = train_model(model, train_dataset_gray, val_dataset_gray)
