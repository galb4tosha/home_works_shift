from pathlib import Path

from tensorflow.keras.losses import BinaryCrossentropy
from tensorflow.keras.metrics import AUC

DF_TRAIN = Path("data") / "raw" / "train.csv"
TRAIN_PATH = Path("data") / "raw" / "train" / "train"
TEST_PATH = Path("data") / "raw" / "test" / "test"
CHECKPOINT_MODEL = Path("models") / "save_model" / "best_model.h5"


TRAIN_VAL_SPLIT = 0.8
IMG_SIZE = (640, 640, 1)
BATCH_SIZE = 64
EPOCHS = 3
FIRST_LAYER_FILTERS = 32
SECOND_LAYER_FILTERS = 16
THIRD_LAYER_FILTERS = 8


# model hyperparametrs
CNN_KERNEL_SIZE = (3, 3)
FILTERS = [32, 16, 8]
CNN_LAYERS_ACTIVATION = "relu"
DENSE_LAYER_ACTIVATION = "sigmoid"

MODEL_LOSS = BinaryCrossentropy()
OPTIMIZER = "adam"
TRACKED_METRICS = ["accuracy", AUC()]
USE_WANDB = False

CHECKPOINT_MONITOR = "val_auc"
CHECKPOINT_MODE = "max"
