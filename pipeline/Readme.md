Чтобы запустить обучение скрипт `python -m src.train`.

Чтобы запустить валидацию скрипт `python -m src.validate`.

Логи можно посмотреть в https://wandb.ai/galb4tosha/pipeline/overview?workspace=user-galb4tosha
